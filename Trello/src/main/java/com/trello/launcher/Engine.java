package com.trello.launcher;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.apache.http.HttpException;
import org.apache.http.client.HttpClient;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;

import com.ning.http.util.Base64;
import com.trello.util.Application;
import com.trello.util.LauncherData.Constants;
import com.trello.util.Trello;

public class Engine {
	private static Map<String, String> applicationProperties = null;
	private static Trello trello = null;
	private static StringBuilder startUpBuilder = new StringBuilder();
	private static Logger REPORT = null;
	private static Logger ERROR = null;
	private static String _trelloUser = null;
	private static String _trelloPwd = null;
	private static String auth = null;
	private static HttpClient httpClient = null;
	static {
		applicationProperties = Application.getApplicationProperties(new File(
				Constants.APPFILE));
		REPORT = Logger.getLogger("reportLog");
		ERROR = Logger.getLogger("errorLog");

		_trelloUser = applicationProperties.get(Constants.USER);
		_trelloPwd = applicationProperties.get(Constants.PWD);
		auth = new String(Base64.encode((_trelloUser + ":" + _trelloPwd)
				.getBytes()));
		httpClient = new org.apache.http.impl.client.DefaultHttpClient();
		trello = Trello.getInstance(applicationProperties, auth, httpClient);
		startUpBuilder
				.append("***********************************************************************************************************\r\n");
		startUpBuilder.append("\t\t\t Trello Autopilot Started\n");
		startUpBuilder
				.append("\t\t\t Please wait...Fetching Cards From Trello \n");
		startUpBuilder.append("\t\t\t Version "
				+ applicationProperties.get(Constants.VERSION) + "\n");
		startUpBuilder
				.append("***********************************************************************************************************");
	}

	public static void main(String[] args) throws HttpException, IOException,
			JSONException, ParseException {
		Date started = Application.started();
		REPORT.info(startUpBuilder);
		// Get number of cards present in given board
		Map<String, String> cards = trello.getCards(applicationProperties);
		REPORT.info("Total Number of card(s) found for board "
				+ applicationProperties.get(Constants.BOARD) + " is "
				+ cards.size() + "\n" + cards);
		/*
		 * Get Movement of each card from In Progress to Done and time taken in
		 * travelling
		 */
		trello.executeCardMovement(cards);
		Application.ended(applicationProperties, started);

	}

}
