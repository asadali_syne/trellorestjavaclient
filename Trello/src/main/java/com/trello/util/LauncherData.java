package com.trello.util;

public class LauncherData {

	public static class Constants {

		public static final String URL = "sync.trello.url";
		public static final String USER = "sync.trello.user";
		public static final String PWD = "sync.trello.password";
		public static final String APPFILE = "application.properties";
		public static final String BOARD = "sync.trello.board";
		public static final String METHOD_DELETE = "DELETE";
		public static final String METHOD_GET = "GET";
		public static final String METHOD_POST = "POST";
		public static final String METHOD_PUT = "PUT";
		public static final String GZIP_ENCODING = "gzip";
		public static final String BOARDURL = "/1/boards/";
		public static final String VERSION = "sync.trello.version";
		public static final String DATEFORMAT12 = "yyyy/MM/dd hh:mm a";
		public static final String IST = "IST";
		public static final String LASTRUN = "sync.trello.lastRun";
		public static final String SHORTLINK = "sync.trello.shortLINK";

	}
}
