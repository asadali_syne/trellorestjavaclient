package com.trello.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;

import org.apache.log4j.Logger;

import com.google.common.collect.Maps;
import com.trello.util.LauncherData.Constants;

public class Application {
	static Properties properties = new Properties();
	public static final Logger REPORT = Logger.getLogger("reportLog");
	public static final Logger ERROR = Logger.getLogger("errorLog");

	public static Map<String, String> getApplicationProperties(File file) {

		Map<String, String> param = new HashMap<String, String>();
		InputStream stream = null;
		try {
			stream = new FileInputStream(file.getAbsolutePath());

			properties.load(stream);
			param = Maps.fromProperties(properties);
		} catch (IOException e) {
			ERROR.info(Level.INFO, e);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				ERROR.info(Level.INFO, e);

			}
		}
		return param;
	}

	public static Date started() {
		Date startDate = new Date();
		DateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy/MM/dd hh:mm:ss a");
		REPORT.info("Started : " + simpleDateFormat.format(startDate));
		ERROR.info("Started : " + simpleDateFormat.format(startDate));

		return startDate;
	}

	public static String ended(Map<String, String> applicationProperties,
			Date started) {
		DateFormat simpleDateFormat = new SimpleDateFormat(
				Constants.DATEFORMAT12);
		Date endDate = new Date();
		long difference = getDifferenceInSeconds(started, endDate);
		long minutes = difference % 3600L / 60L;
		if (minutes < 3) {
			minutes = 3;
		}

		simpleDateFormat.setTimeZone(TimeZone.getTimeZone(Constants.IST));
		updateAppProperty(applicationProperties, started, difference);

		String duration = getDurationString(difference);

		REPORT.info("--------------------------------API Execution Time " + duration+"------------------------------------------");
		ERROR.info("--------------------------------API Execution Time " + duration+"------------------------------------------");
		return duration;

	}

	private static String getDurationString(long seconds) {
		long hours = seconds / 3600L;
		long minutes = seconds % 3600L / 60L;
		seconds %= 60L;
		return twoDigitString(hours) + "h : " + twoDigitString(minutes)
				+ "m : " + twoDigitString(seconds) + "s";
	}

	private static String twoDigitString(long number) {
		if (number == 0L)
			return "00";
		if (number / 10L == 0L)
			return "0" + number;
		return String.valueOf(number);
	}

	private static void updateAppProperty(
			Map<String, String> applicationProperties, Date started,
			long difference) {
		DateFormat simpleDateFormat = new SimpleDateFormat(
				Constants.DATEFORMAT12);

		Date vLastUpdated = new Date();
		TimeZone ist = TimeZone.getTimeZone(Constants.IST);
		simpleDateFormat.setTimeZone(ist);
		setLastUpdated(Constants.LASTRUN, simpleDateFormat.format(vLastUpdated));

	}

	private static void setLastUpdated(String key, String lastUpdated) {
		if (!(isBlankOrNull(lastUpdated))) {
			properties.setProperty(key, lastUpdated);
			try {
				File updatedFile = new File(Constants.APPFILE);
				FileOutputStream fileOut = new FileOutputStream(updatedFile);
				properties.store(fileOut, key);
				fileOut.close();
			} catch (IOException e) {
				// e.printStackTrace();
				ERROR.info(Level.INFO, e);
			}
		}

	}

	private static boolean isBlankOrNull(Object object) {
		return (object == null || "".equalsIgnoreCase(object.toString().trim()));
	}

	private static long getDifferenceInSeconds(Date started, Date endDate) {
		return (endDate.getTime() - started.getTime()) / 1000;
	}

}
