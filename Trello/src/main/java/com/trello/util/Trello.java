package com.trello.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.trello.util.LauncherData.Constants;

public class Trello {
	private static Map<String, String> applicationProperties;
	private static Trello trello = null;
	public static final Logger REPORT = Logger.getLogger("reportLog");
	public static final Logger ERROR = Logger.getLogger("errorLog");
	private static String auth = null;
	private static HttpClient httpClient = null;

	private Trello(Map<String, String> applicationProperties, String auth,
			HttpClient httpClient) {
		this.applicationProperties = applicationProperties;
		this.auth = auth;
		this.httpClient = httpClient;
	}

	public static Trello getInstance(Map<String, String> applicationProperties,
			String auth, HttpClient httpClient) {
		if (trello == null) {
			return new Trello(applicationProperties, auth, httpClient);
		}
		return null;

	}

	public Map<String, String> getCards(
			Map<String, String> applicationProperties2) {
		String boardURL = applicationProperties.get(Constants.URL)
				+ Constants.BOARDURL
				+ applicationProperties.get(Constants.SHORTLINK);
		// Invoking GET Method by using short link in URL to retrieve board id
		String boardId = getBoard(getJSONData(boardURL));
		// Preparing URL to get all card for retrieved board id
		String cardsURL = applicationProperties.get(Constants.URL)
				+ Constants.BOARDURL + boardId + "/cards";
		/*
		 * Retrieve & return all card present in given board by invoking GET
		 * method of Rest
		 */
		Map<String, String> cards = getCards(prepareJSONData(
				getJSONData(cardsURL)).trim());
		return cards;

	}

	public void executeCardMovement(Map<String, String> cards) {

		Iterator<Entry<String, String>> iterateCards = cards.entrySet()
				.iterator();
		while (iterateCards.hasNext()) {
			Entry<String, String> entryCards = iterateCards.next();
			String id = entryCards.getKey();
			String eachCardJsondata = getJSONData(applicationProperties
					.get(Constants.URL)
					+ "/1/cards/"
					+ id
					+ "?actions=updateCard");
			getcardWithDone(id, entryCards.getValue(), eachCardJsondata);

		}

	}

	private static String getcardWithDone(String id, String value,
			String eachCardJsondata) {
		JSONObject jsonObj;
		String cardURL = null;

		try {
			jsonObj = new JSONObject(eachCardJsondata);
			/*
			 * Get details of actions performed on each card i.e., status before
			 * and after
			 */
			JSONArray actions = jsonObj.getJSONArray("actions");
			if (actions.get(0) != null) {
				String data = actions.getJSONObject(0).get("data").toString();
				jsonObj = new JSONObject(data);
				if ("Done".equalsIgnoreCase(jsonObj.getJSONObject("listAfter")
						.get("name").toString()))

				{
					REPORT.info("Card " + value + " : " + id
							+ " reached to Done");
					cardURL = applicationProperties.get(Constants.URL)
							+ "/1/cards/" + id + "?actions=updateCard";
					getTimeTravelled(getJSONData(cardURL), id, value);
					return id;
				}
			}

		} catch (JSONException e) {

			ERROR.info(Level.INFO, e);
		}
		return null;

	}

	private static String prepareJSONData(String cardsJSONData) {
		if (cardsJSONData != null)
			return "{\"data\":" + cardsJSONData.concat("}");
		return null;
	}

	public static Map<String, String> getCards(String cardsJSONData) {
		JSONObject jsonObj;
		String id = null;
		String name = null;
		Map<String, String> hashMap = new HashMap<String, String>();
		try {
			jsonObj = new JSONObject(cardsJSONData);
			/*
			 * Adding name and id of each card present in given board to HashMap
			 * and Returning
			 */
			int size = jsonObj.getJSONArray("data").length();
			for (int i = 0; i < size; i++) {
				name = jsonObj.getJSONArray("data").getJSONObject(i)
						.getString("name");

				id = jsonObj.getJSONArray("data").getJSONObject(i)
						.getString("id");
				hashMap.put(id, name);

			}

		} catch (JSONException e) {
			ERROR.info(Level.INFO, e);

		}
		return hashMap;
	}

	private static String getBoard(String boardData) {
		JSONObject jsonObj;
		String id = null;
		String name = null;
		try {
			jsonObj = new JSONObject(boardData);

			name = jsonObj.getString("name");

			id = jsonObj.getString("id");

		} catch (JSONException e) {
			ERROR.info(Level.INFO, e);

		}
		if (name.equalsIgnoreCase(applicationProperties.get(Constants.BOARD)))
			return id;
		return null;

	}

	private static String getTimeTravelled(String jsonData, String id,
			String name) {
		JSONObject jsonObj;

		List<String> upDate = new ArrayList<String>();
		try {
			jsonObj = new JSONObject(jsonData);
			/*
			 * Retrieving date at the time of "In Progress" and date at the time
			 * of "Done" from JSon
			 */
			int size = jsonObj.getJSONArray("actions").length();
			for (int i = 0; i < size; i++) {
				upDate.add(jsonObj.getJSONArray("actions").getJSONObject(i)
						.getString("date"));

			}
		} catch (JSONException e) {
			ERROR.info(Level.INFO, e);
		}
		String ISO_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat format = new SimpleDateFormat(ISO_FORMAT);

		Date firstDate = null;
		Date lastDate = null;
		try {
			firstDate = format.parse(upDate.get(upDate.size() - 1).replaceAll(
					"T", " "));
			lastDate = format.parse(upDate.get(0).replaceAll("T", " "));
		} catch (ParseException e) {
			ERROR.info(Level.INFO, e);

		}
		long diff = lastDate.getTime() - firstDate.getTime();
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000);
		String timeTaken = "Time taken to travel from In Progress to Done by card "
				+ name
				+ " is : \n"
				+ diffHours
				+ " hours. "
				+ diffMinutes
				+ " minutes. " + diffSeconds + " seconds.";
		REPORT.info(timeTaken);
		return timeTaken;
	}

	private static String getJSONData(String url) {

		HttpGet post = new HttpGet(url);
		post.addHeader("Content-type", "application/json; charset=utf-8");

		post.addHeader("Authorization", "Basic " + auth);
		HttpResponse response = null;
		String content = null;
		try {
			response = httpClient.execute(post);
			content = CharStreams.toString(new InputStreamReader(response
					.getEntity().getContent(), Charsets.UTF_8));

		} catch (IOException e) {

			ERROR.info(Level.INFO, e);
		}
		return content;
	}

}
