package com.trello.beans;

import java.util.List;

public class Actions {
	private String id;
	private List<Badges> badges;
	private List<Action> actions;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Badges> getBadges() {
		return badges;
	}

	public void setBadges(List<Badges> badges) {
		this.badges = badges;
	}

	@Override
	public String toString() {
		return "Actions [id=" + id + ", badges=" + badges + ", actions="
				+ actions + ", getId()=" + getId() + ", getBadges()="
				+ getBadges() + ", getActions()=" + getActions()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

}
