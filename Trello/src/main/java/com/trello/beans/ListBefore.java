package com.trello.beans;

public class ListBefore {
	private String name;

	@Override
	public String toString() {
		return "ListBefore [name=" + name + ", getName()=" + getName()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
