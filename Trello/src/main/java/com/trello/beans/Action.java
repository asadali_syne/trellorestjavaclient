package com.trello.beans;

import java.util.List;

public class Action {
	private List<ListBefore> listBefore;
	private List<String> date;

	public List<String> getDate() {
		return date;
	}

	public void setDate(List<String> date) {
		this.date = date;
	}

	public void setListBefore(List<ListBefore> listBefore) {
		this.listBefore = listBefore;
	}

	public void setListAfter(List<ListAfter> listAfter) {
		this.listAfter = listAfter;
	}

	public void setCard(List<Card> card) {
		this.card = card;
	}

	private List<ListAfter> listAfter;
	private List<Card> card;

	@Override
	public String toString() {
		return "Action [listBefore=" + listBefore + ", date=" + date
				+ ", listAfter=" + listAfter + ", card=" + card
				+ ", getDate()=" + getDate() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
