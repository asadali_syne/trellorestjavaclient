# TRJC
TrelloRestJavaClient

**Trello API** - Calculate how long did it take for each card to be done

https://trello.com/b/CMV0pT47/kanban is a public Trello board with some dummy data in it

Using the Trello API - https://developers.trello.com/apis ..
Calculate how long did it take for each card to get from "In Progress" to "Done" using any server side language/framework.


##Trello API
   Target Release 	      - 	 0.0.1
   Document Owner         -  	Asad Ali
   Designer	               -  	Asad Ali
   Developer		            -  	Asad Ali
   Technology               -        Java

 **Goal - Calculate how long did it take for each card to be done** 

 ##Step – 1:
	Create(if it is new else import as maven) a maven Project with any IDE, in my case its Eclipse.
 ##Step – 2: Maven Dependencies-
Add below dependencies to your POM
	
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpcore</artifactId>
			<version>4.4.3</version>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.17</version>
		</dependency>
		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
			<version>1.1.1</version>
		</dependency>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>r05</version>
		</dependency>

 ##Step – 3:Configuration-
Create/find “application.properties” file,  input all parameters, which is bridge between two instances. Here it looks like below.,

	sync.trello.url=https\://trello.com
	sync.trello.version=0.0.1
	sync.trello.password=XXXX
	sync.trello.board=Kanban
	sync.trello.lastRun=2016/05/19 08\:02 PM
	sync.trello.user=XXXX
	sync.trello.shortLINK=CMV0pT47

 ##Step – 4: Add other jar(s) under custom Library
1.async-http-client-1.6.1.jar
2.trello4j-1.0-2013.10.19.jar
3.jettison-1.3.jar\jettison-1.3.jar
4.httpclient-4.2.3.jar\httpclient-4.2.3.jar

   ##Step – 5: Configure log4j.properties, Here it looks like, 

log4j.rootLogger=INFO, stdout

## Log to console
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%m%n

log4j.appender=INFO,reportLog
log4j.appender.reportLog=org.apache.log4j.ConsoleAppender
log4j.appender.reportLog=org.apache.log4j.FileAppender
log4j.appender.reportLog.File=logs/reports.log
log4j.appender.reportLog.layout=org.apache.log4j.PatternLayout
log4j.appender.reportLog.layout.ConversionPattern=%m%n

log4j.appender=TRACE,errorLog
log4j.appender.errorLog=org.apache.log4j.FileAppender
log4j.appender.errorLog.File=logs/errors.log
log4j.appender.errorLog.layout=org.apache.log4j.PatternLayout
log4j.appender.errorLog.layout.ConversionPattern=%m%n
log4j.logger.org.springframework=OFF

log4j.category.reportLog=INFO, reportLog
log4j.additivity.reportLog=true

log4j.category.errorLog=TRACE, errorLog
log4j.additivity.errorLog=false

 ##Step – 6:
 Run as java application, and check logs under logs folder.

 # Design Pattern, Classes and Methods Overview

-Singleton Pattern

-Compile time loading of reference/instances.

-Trello class to process Trello realted activities.

-Application class for loading/updating appliction.proprties file.

-Classes/methods invocation  from Launch class which contains main method. 

##Implementation-

-getCards() method of Trello class will return the number of cards present in given board(board name and short URL need to specify in appliction.properties file.

-ExecuteCardMovement() method of Trello class will calculate and return total time taken by each card to travell from “In Progress” to “Done”.

-Methods present in Trello/Launch/Application classes are logged properly under reports.log as well as Exceptions will be  logged under errors.log(in case any NullPointerException etc).